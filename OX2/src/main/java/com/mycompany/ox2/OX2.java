/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.ox2;

import java.util.Scanner;

/**
 *
 * @author ASUS
 */
public class OX2 {

    static char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static char currentPlayer = 'X';
    static int row, col;
    static String yn;
    static boolean True = true;

    public static void main(String[] args) {
        printWelcome();
        while (True) {
            printTable();
            printTurn();
            inputRowCol();

            if (isWin()) {
                printTable();
                printWinner();
                inputContinue();
            }
            if (checkDraw()) {
                printTable();
                printDraw();
                inputContinue();
            }
            switchPleyer();
        }
    }

    private static void printWelcome() {
        System.out.println("Welcome to OX");
    }

    private static void printTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println("");
        }
    }

    private static void printTurn() {
        System.out.println(currentPlayer + " Turn");
    }

    private static void inputRowCol() {
        Scanner kb = new Scanner(System.in);
        while (true) {
            System.out.print("Please input row,col: ");
            row = kb.nextInt();
            col = kb.nextInt();
            if (table[row - 1][col - 1] == '-') {
                table[row - 1][col - 1] = currentPlayer;
                return;
            }
        }
    }

    private static void switchPleyer() {
        if (currentPlayer == 'X') {
            currentPlayer = 'O';
        } else {
            currentPlayer = 'X';
        }
    }

    private static boolean isWin() {
        if (checkRow()) {
            return true;
        } else if (checkCol()) {
            return true;
        } else if (checkX1()) {
            return true;
        } else if (checkX2()) {
            return true;
        }
        return false;
    }

    private static void printWinner() {
        System.out.println(currentPlayer + " Winner !!");
    }

    private static boolean checkRow() {
        for (int i = 0; i < 3; i++) {
            if (table[row - 1][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkCol() {
        for (int i = 0; i < 3; i++) {
            if (table[i][col - 1] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX1() {
        for (int i = 0; i < 3; i++) {
            if (table[i][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX2() {
        for (int i = 0; i < 3; i++) {
            if (table[i][2 - i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkDraw() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }

    private static void printDraw() {
        System.out.println("Draw");
    }

    private static void EndGame() {
        System.out.println("End Game");
    }

    private static void ResetTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                table[i][j] = '-';
            }
        }
    }

    private static boolean inputContinue() {
        System.out.println("Continue? (y/n)");
        Scanner sc = new Scanner(System.in);
        yn = sc.next();
        if (yn.equals("y")) {
            ResetTable();
            Restart();

        } else if (yn.equals("n")) {
            EndGame();
            True = false;
        }
        return true;
    }
    
        private static void Restart() {
        printWelcome();
        while (True) {
            printTable();
            printTurn();
            inputRowCol();
            if (isWin()) {
                printTable();
                printWinner();
                inputContinue();
            }
            if (checkDraw()) {
                printTable();
                printDraw();
                inputContinue();
            }
            switchPleyer();
        }
    }

}
